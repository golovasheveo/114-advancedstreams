package telran.util;

import java.util.Random;
import java.util.stream.Collectors;

class AdvancedStreams {
    public static void main ( String[] args ) {
        displayDigitStatistics ( );
    }


    private static void displayDigitStatistics ( ) {
        new Random ( )
                .ints (1, Integer.MAX_VALUE )
                .distinct ( )
                .limit ( 1000000 )
                .flatMap ( n -> Integer.toString ( n ).chars ( ) )
                .boxed ( )
                .collect ( Collectors.groupingBy ( n -> n, Collectors.counting ( ) ) )
                .entrySet ( )
                .stream ( )
                .sorted ( ( a, b ) -> Long.compare ( a.getValue ( ), b.getValue ( ) ) )
                .forEach ( res -> {
                    System.out.printf ( "%c : %d\n", res.getKey (), res.getValue () );
                } );
    }

}
